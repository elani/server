﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
//using Test.TestServer;
using Test;
using Server;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            WebServiceHost host = new WebServiceHost(typeof(FileManagerService), new Uri("http://localhost:8000/"));
            try
            {
                ServiceEndpoint ep = host.AddServiceEndpoint(typeof(IFileManagerService), new WebHttpBinding(), "");
                host.Open();
                using (var cf = new ChannelFactory<IFileManagerService>(new WebHttpBinding(), "http://localhost:8000/"))
                {
                    cf.Endpoint.Behaviors.Add(new WebHttpBehavior());

                    IFileManagerService channel = cf.CreateChannel();


                }

                Console.WriteLine("Press <ENTER> to terminate");
                Console.ReadLine();

                //host.Close();
            }
            catch (CommunicationException cex)
            {
                Console.WriteLine("An exception occurred: {0}", cex.Message);
                host.Abort();
            }
        }
    }
}
