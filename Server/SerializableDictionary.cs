﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Server
{
    [Serializable]
    public class SerializableDictionary<TKey, TValue> : ISerializable
    {
        private Dictionary<TKey, TValue> _Dictionary;
        public SerializableDictionary()
        {
            _Dictionary = new Dictionary<TKey, TValue>();
        }
        public SerializableDictionary(SerializationInfo info, StreamingContext context)
        {
            _Dictionary = new Dictionary<TKey, TValue>();
        }
        public TValue this[TKey key]
        {
            get { return _Dictionary[key]; }
            set { _Dictionary[key] = value; }
        }
        public void Add(TKey key, TValue value)
        {
            _Dictionary.Add(key, value);
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            foreach (TKey key in _Dictionary.Keys)
                info.AddValue(key.ToString(), _Dictionary[key]);
        }
    }
}