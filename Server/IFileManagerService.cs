﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFileManagerService" in both code and config file together.
    [ServiceContract]
    public interface IFileManagerService
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "/directory_info")]
        DirectoryContract GetDirectoryContent(string path);

        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "/root_directory")]
        DirectoryContract GetRootPath();
        

    }

    [DataContract(Name = "directory")]
    public class DirectoryContract
    {
        [DataMember]
        public string Path { get; set; }
        [DataMember]
        public List<string> Directories { get; set; }
        [DataMember]
        public List<string> Files { get; set; }
    }
}
