﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Server
{
    public static class FileSystemDataSource
    {
        public static Dictionary<string, List<string>> GetContent(string directoryPath)
        {
            var dInfo = new DirectoryInfo(directoryPath);
            var dirs = new List<string>();
            foreach (var item in dInfo.GetDirectories())
            {
                dirs.Add(item.Name);
            }
            var files = new List<string>();
            foreach (var item in dInfo.GetFiles())
            {
                files.Add(item.Name);
            }
            var dict = new Dictionary<string, List<string>>();
            dict.Add("Directories", dirs);
            dict.Add("Files", files);
            return dict;
        }
        public static string GetRootPath()
        {
            return "D:\\Games";//в будущем инфа будет браться из конфига
        }
        
    }
}