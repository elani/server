﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FileManagerService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select FileManagerService.svc or FileManagerService.svc.cs at the Solution Explorer and start debugging.
    public class FileManagerService : IFileManagerService
    {
        public DirectoryContract GetDirectoryContent(string path)
        {
            var content = FileSystemDataSource.GetContent(path);
            if (content == null)
            {
                return null;
            }
            var directory = new DirectoryContract()
            {
                Path = path,
                Directories = content["Directories"],
                Files = content["Files"]
            };
            return directory;
        }
        public DirectoryContract GetRootPath()
        {
            var path = FileSystemDataSource.GetRootPath();
            return GetDirectoryContent(path);
        }
    }
}
