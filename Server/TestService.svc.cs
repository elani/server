﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class TestService : ITestService
    {
        public TestType GetData(string value)
        {

            var str = string.Format("You entered: {0}", value);
            string[] s = { str, "jkfksdj" };
            TestType tst = new TestType();
            var dict = new Dictionary<string, string>();
            dict.Add("response", str);
            //dict["response"] = str;
            tst.DictionaryValue = dict;
            tst.StringValue = str;
            return tst;
        }

        public string PostData(TestType value)
        {
            
            return null;
        }
        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
