﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ITestService
    {

        [OperationContract]
        [WebInvoke(Method="GET",
            ResponseFormat=WebMessageFormat.Json)]
        TestType GetData(string value);

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat=WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        string PostData(TestType value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
    [DataContract]
    public class TestType
    {
        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        string stringValue = "Hello ";

        [DataMember]
        public Dictionary<string, string> DictionaryValue
        {
            get { return dictionary; }
            set { dictionary = value; }
        }
        [DataMember]
        public string StringValue
       {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
